package com.java.common.exception;

import com.java.common.constants.ResultCodeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author jiangli
 * @since 2019/6/25
 * 自定义全局异常类
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RRException extends RuntimeException {

	private Integer code;

	public RRException(String msg) {
		super(msg);
	}

	/**
	 * 接收状态码和错误消息
	 * @param code
	 * @param message
	 */
	public RRException(Integer code, String message){
		super(message);
		this.code = code;
	}

	public RRException(ResultCodeEnum resultCodeEnum){
		super(resultCodeEnum.getMessage());
		this.code = resultCodeEnum.getCode();
	}

	@Override
	public String toString() {
		return "RRException{" +
				"code=" + code +
				", message=" + this.getMessage() +
				'}';
	}
}
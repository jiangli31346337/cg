package com.java.oauth.util;

import java.io.Serializable;

/****
 * 用户令牌封装
 *****/
public class AuthToken implements Serializable{

    //令牌信息
    private String access_token;
    //刷新token(refresh_token)
    private String refresh_token;
    //jwt短令牌
    private String jti;

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

	public String getJti() {
		return jti;
	}

	public void setJti(String jti) {
		this.jti = jti;
	}
}
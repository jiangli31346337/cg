package com.java.oauth.service;

import com.java.oauth.util.AuthToken;

public interface AuthService {

	/***
	 * 授权认证方法
	 */
	AuthToken login(String username, String password);
}

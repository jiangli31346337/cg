package com.java.cg.order.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.cg.order.entity.OrderLog;
import com.java.cg.order.service.OrderLogService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 
 *
 * @author jiangli
 * @since 2020-02-17 19:17:37
 */
@RestController
@RequestMapping("order/orderlog")
public class OrderLogController {
    @Autowired
    private OrderLogService orderLogService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = orderLogService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") String id){
		OrderLog orderLog = orderLogService.getById(id);

        return R.ok().put("orderLog", orderLog);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody OrderLog orderLog){
		orderLogService.save(orderLog);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody OrderLog orderLog){
		orderLogService.updateById(orderLog);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] ids){
		orderLogService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}

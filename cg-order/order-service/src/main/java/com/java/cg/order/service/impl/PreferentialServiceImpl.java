package com.java.cg.order.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.order.dao.PreferentialDao;
import com.java.cg.order.entity.Preferential;
import com.java.cg.order.service.PreferentialService;

@Service("preferentialService")
public class PreferentialServiceImpl extends ServiceImpl<PreferentialDao, Preferential> implements PreferentialService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Preferential> page = this.page(
                new Query<Preferential>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}
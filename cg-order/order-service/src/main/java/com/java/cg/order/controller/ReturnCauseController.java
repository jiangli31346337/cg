package com.java.cg.order.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.cg.order.entity.ReturnCause;
import com.java.cg.order.service.ReturnCauseService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 
 *
 * @author jiangli
 * @since 2020-02-17 19:17:37
 */
@RestController
@RequestMapping("order/returncause")
public class ReturnCauseController {
    @Autowired
    private ReturnCauseService returnCauseService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = returnCauseService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id){
		ReturnCause returnCause = returnCauseService.getById(id);

        return R.ok().put("returnCause", returnCause);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody ReturnCause returnCause){
		returnCauseService.save(returnCause);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody ReturnCause returnCause){
		returnCauseService.updateById(returnCause);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] ids){
		returnCauseService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}

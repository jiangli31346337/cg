package com.java.cg.order.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.order.dao.ReturnOrderDao;
import com.java.cg.order.entity.ReturnOrder;
import com.java.cg.order.service.ReturnOrderService;

@Service("returnOrderService")
public class ReturnOrderServiceImpl extends ServiceImpl<ReturnOrderDao, ReturnOrder> implements ReturnOrderService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ReturnOrder> page = this.page(
                new Query<ReturnOrder>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}
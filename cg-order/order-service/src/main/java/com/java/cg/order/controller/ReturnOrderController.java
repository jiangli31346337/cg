package com.java.cg.order.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.cg.order.entity.ReturnOrder;
import com.java.cg.order.service.ReturnOrderService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 
 *
 * @author jiangli
 * @since 2020-02-17 19:17:36
 */
@RestController
@RequestMapping("order/returnorder")
public class ReturnOrderController {
    @Autowired
    private ReturnOrderService returnOrderService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = returnOrderService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		ReturnOrder returnOrder = returnOrderService.getById(id);

        return R.ok().put("returnOrder", returnOrder);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody ReturnOrder returnOrder){
		returnOrderService.save(returnOrder);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody ReturnOrder returnOrder){
		returnOrderService.updateById(returnOrder);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		returnOrderService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}

package com.java.cg.order.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.order.dao.OrderConfigDao;
import com.java.cg.order.entity.OrderConfig;
import com.java.cg.order.service.OrderConfigService;

@Service("orderConfigService")
public class OrderConfigServiceImpl extends ServiceImpl<OrderConfigDao, OrderConfig> implements OrderConfigService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderConfig> page = this.page(
                new Query<OrderConfig>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}
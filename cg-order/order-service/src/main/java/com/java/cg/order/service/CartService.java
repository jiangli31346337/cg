package com.java.cg.order.service;

import com.java.cg.order.entity.OrderItem;

import java.util.List;

/**
 * @author jiangli
 * @since 2020/2/17 19:34
 */
public interface CartService {

	void addCart(Integer count, Long skuId, String username);

	List<OrderItem> list(String username);

	void delete(Long skuId, String username);
}

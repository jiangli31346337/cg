package com.java.cg.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.common.utils.PageUtils;
import com.java.cg.order.entity.Order;

import java.text.ParseException;
import java.util.Map;

/**
 * 
 *
 * @author jiangli
 * @since 2020-02-17 19:17:37
 */
public interface OrderService extends IService<Order> {

    PageUtils queryPage(Map<String, Object> params);

	void addOrder(Order order);

	void updateOrderStatus(String orderId,String transactionId,String payTime);

	Integer closeOrder(String orderId);
}


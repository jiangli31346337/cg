package com.java.cg.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.common.utils.PageUtils;
import com.java.cg.order.entity.ReturnCause;

import java.util.Map;

/**
 * 
 *
 * @author jiangli
 * @since 2020-02-17 19:17:37
 */
public interface ReturnCauseService extends IService<ReturnCause> {

    PageUtils queryPage(Map<String, Object> params);
}


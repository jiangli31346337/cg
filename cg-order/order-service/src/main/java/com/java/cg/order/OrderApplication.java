package com.java.cg.order;

import com.java.common.config.FeignInterceptor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

/**
 * @author jiangli
 * @since 2020/2/17 19:24
 */
@SpringBootApplication
@EnableFeignClients(basePackages = {"com.java.cg.goods.feign","com.java.cg.user.feign"})
public class OrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderApplication.class,args);
	}

	/**
	 * 将feign调用拦截器注入到容器中
	 */
	@Bean
	public FeignInterceptor feignInterceptor() {
		return new FeignInterceptor();
	}
}

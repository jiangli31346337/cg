package com.java.cg.order.dao;

import com.java.cg.order.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-17 19:17:37
 */
@Mapper
public interface OrderDao extends BaseMapper<Order> {

	//订单表的状态设计不完善
	@Update("UPDATE `tb_order` SET `is_delete` = '1' WHERE `id` = #{orderId} AND `is_delete` = '0'")
	Integer closeOrder(String orderId);
	
}

package com.java.cg.order.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.cg.order.entity.ReturnOrderItem;
import com.java.cg.order.service.ReturnOrderItemService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 
 *
 * @author jiangli
 * @since 2020-02-17 19:17:36
 */
@RestController
@RequestMapping("order/returnorderitem")
public class ReturnOrderItemController {
    @Autowired
    private ReturnOrderItemService returnOrderItemService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = returnOrderItemService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		ReturnOrderItem returnOrderItem = returnOrderItemService.getById(id);

        return R.ok().put("returnOrderItem", returnOrderItem);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody ReturnOrderItem returnOrderItem){
		returnOrderItemService.save(returnOrderItem);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody ReturnOrderItem returnOrderItem){
		returnOrderItemService.updateById(returnOrderItem);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		returnOrderItemService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}

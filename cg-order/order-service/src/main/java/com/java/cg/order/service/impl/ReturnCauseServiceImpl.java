package com.java.cg.order.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.order.dao.ReturnCauseDao;
import com.java.cg.order.entity.ReturnCause;
import com.java.cg.order.service.ReturnCauseService;

@Service("returnCauseService")
public class ReturnCauseServiceImpl extends ServiceImpl<ReturnCauseDao, ReturnCause> implements ReturnCauseService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ReturnCause> page = this.page(
                new Query<ReturnCause>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}
package com.java.file;

import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.domain.ThumbImageConfig;
import com.github.tobato.fastdfs.proto.storage.DownloadByteArray;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FdfsTest {

    @Autowired
    private FastFileStorageClient storageClient;

    @Autowired
    private ThumbImageConfig thumbImageConfig;

    //普通上传
    @Test
    public void testUpload() throws FileNotFoundException {
        File file = new File("C:\\Users\\JavaDev\\Desktop\\she.jpg");
        // 上传
        StorePath storePath = this.storageClient.uploadFile(
                new FileInputStream(file), file.length(), "jpg", null);
        // 带分组的路径
        System.out.println(storePath.getFullPath());
        // 不带分组的路径
        System.out.println(storePath.getPath());
    }

    //上传并创建缩略图
    @Test
    public void testUploadAndCreateThumb() throws FileNotFoundException {
        File file = new File("D:\\image\\1.jpg");
        // 上传并且生成缩略图
        StorePath storePath = this.storageClient.uploadImageAndCrtThumbImage(
                new FileInputStream(file), file.length(), "jpg", null);
        // 带分组的路径
        System.out.println(storePath.getFullPath());
        // 不带分组的路径
        System.out.println(storePath.getPath());
        // 获取缩略图路径
        String path = thumbImageConfig.getThumbImagePath(storePath.getPath());
        System.out.println(path);
    }

    //文件下载
    @Test
    public void downLoad() throws Exception{
        String fileUrl = "group1/M00/00/00/rBEADV2-f2OAVB_iAANNFAnS3TE454.png";
        String group = fileUrl.substring(0, fileUrl.indexOf("/"));
        String path = fileUrl.substring(fileUrl.indexOf("/") + 1);
        DownloadByteArray downloadByteArray = new DownloadByteArray();
        byte[] content = storageClient.downloadFile(group, path, downloadByteArray);
        InputStream inputStream = new ByteArrayInputStream(content);
        FileOutputStream fileOutputStream = new FileOutputStream(new File("d:/logo.png"));
        IOUtils.copy(inputStream,fileOutputStream);

        //通过response响应给前端
/*        if (content == null || content.length == 0) {
//                log.error("文件大小为空！");
            return;
        }
        response.setCharacterEncoding("UTF-8");
        response.setHeader("content-Type", "application/octet-stream");
        response.setHeader("Content-Disposition","attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
        response.getOutputStream().write(content);
        */
    }

    // 删除文件
	@Test
	public void delete() {
		storageClient.deleteFile("group1/M00/00/00/rBEADV2-f2OAVB_iAANNFAnS3TE454_80x80.png");
	}
}
package com.java.cg.item.controller;

import com.java.cg.item.service.ItemService;
import com.java.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jiangli
 * @since 2020/2/15 16:54
 */
@RestController
@RequestMapping("item")
public class ItemController {
	@Autowired
	private ItemService itemService;

	/**
	 * 生成静态页面
	 */
	@RequestMapping("/createHtml/{spuId}")
	public R createHtml(@PathVariable("spuId") Long spuId) {
		itemService.createHtml(spuId);
		return R.ok();
	}

	//访问生成的页面http://localhost:18087/items/1148472451710152704.html
}

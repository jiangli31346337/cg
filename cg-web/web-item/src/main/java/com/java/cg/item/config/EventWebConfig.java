package com.java.cg.item.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author jiangli
 * @since 2020/2/15 19:00
 */
@Configuration
public class EventWebConfig implements WebMvcConfigurer {

	/**
	 * 对templates模板下的静态资源放行
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/items/**") //请求路径的映射
				.addResourceLocations("classpath:/items/");   //本地查找路径
	}
}

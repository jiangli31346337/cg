package com.java.cg.item;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author jiangli
 * @since 2020/2/15 16:31
 */
@SpringBootApplication
@EnableFeignClients(basePackages = {"com.java.cg.goods.feign"})
public class WebItemApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebItemApplication.class,args);
	}
}

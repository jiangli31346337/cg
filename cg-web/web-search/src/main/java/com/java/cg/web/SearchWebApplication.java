package com.java.cg.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author jiangli
 * @since 2020/2/15 11:07
 */
@SpringBootApplication
@EnableFeignClients(basePackages = {"com.java.cg.search.api"})
public class SearchWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(SearchWebApplication.class,args);
	}
}

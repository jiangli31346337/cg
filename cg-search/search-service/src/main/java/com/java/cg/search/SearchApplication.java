package com.java.cg.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author jiangli
 * @since 2020/2/14 14:28
 */
@SpringBootApplication
@EnableFeignClients(basePackages = {"com.java.cg.goods.feign"})
public class SearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(SearchApplication.class,args);
	}
}

package com.java.cg.search.repository;

import com.java.cg.search.entity.SkuInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * @author jiangli
 * @since 2020/2/14 16:51
 */
@Repository
public interface SkuInfoRepository extends ElasticsearchRepository<SkuInfo,Long> {
}

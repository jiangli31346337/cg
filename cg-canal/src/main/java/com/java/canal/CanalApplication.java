package com.java.canal;

import com.xpand.starter.canal.annotation.EnableCanalClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author jiangli
 * @since 2020/2/14 10:39
 */
@SpringBootApplication
@EnableCanalClient
@EnableFeignClients(basePackages = {"com.java.cg.content.feign"})
public class CanalApplication {

	public static void main(String[] args) {
		SpringApplication.run(CanalApplication.class,args);
	}
}

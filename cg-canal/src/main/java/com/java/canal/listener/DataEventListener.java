package com.java.canal.listener;

import com.alibaba.fastjson.JSON;
import com.alibaba.otter.canal.protocol.CanalEntry;
import com.java.cg.content.entity.Content;
import com.java.cg.content.feign.ContentApi;
import com.xpand.starter.canal.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

/**
 * @author jiangli
 * @since 2020/2/14 10:41
 * 实现mysql数据监听
 */
@CanalEventListener
public class DataEventListener {
	@Autowired
	private ContentApi contentApi;
	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	/**
	 * 监听新增 (监听到增加后的数据)
	 *
	 * @param eventType 当前操作的类型 新增数据
	 * @param rowData   发生变更的一行数据
	 */
	@InsertListenPoint
	public void onEventInsert(CanalEntry.EventType eventType, CanalEntry.RowData rowData) {
		for (CanalEntry.Column column : rowData.getAfterColumnsList()) {
			System.out.println("列名:" + column.getName() + "----,变更的数据:" + column.getValue());
		}
	}

	/**
	 * 监听修改
	 */
	@UpdateListenPoint
	public void onEventUpdate(CanalEntry.EventType eventType, CanalEntry.RowData rowData) {
		for (CanalEntry.Column column : rowData.getBeforeColumnsList()) {
			System.out.println("修改前的列名:" + column.getName() + "----,变更的数据:" + column.getValue());
		}
		for (CanalEntry.Column column : rowData.getAfterColumnsList()) {
			System.out.println("修改后的列名:" + column.getName() + "----,变更的数据:" + column.getValue());
		}
	}

	/**
	 * 监听删除
	 */
	@DeleteListenPoint
	public void onEventDelete(CanalEntry.EventType eventType, CanalEntry.RowData rowData) {
		for (CanalEntry.Column column : rowData.getBeforeColumnsList()) {
			System.out.println("删除前的列名:" + column.getName() + "----,变更的数据:" + column.getValue());
		}
	}

     /**
	 * 自定义监听
	 * 实现监听广告的增删改，并根据增删改的数据使用feign查询对应分类的所有广告，将广告存入到Redis中
	 */
	@ListenPoint(
			eventType = {CanalEntry.EventType.INSERT, CanalEntry.EventType.UPDATE, CanalEntry.EventType.DELETE}, // 监听类型
			schema = {"changgou_content"}, // 指定监听的数据库
			table = {"tb_content", "tb_content_category"}, // 指定监听的表
			destination = "example" // 指定实例的地址
	)
	public void onEventCustomUpdate(CanalEntry.EventType eventType, CanalEntry.RowData rowData) {
		//1.获取列名 为category_id的值
		String categoryId = getColumnValue(eventType, rowData);
		//2.调用feign 获取该分类下的所有的广告集合
		List<Content> contents = contentApi.findByCategory(Long.valueOf(categoryId));
		//3.使用redisTemplate存储到redis中
		stringRedisTemplate.boundValueOps("content_" + categoryId).set(JSON.toJSONString(contents));
	}

	private String getColumnValue(CanalEntry.EventType eventType, CanalEntry.RowData rowData) {
		String categoryId = "";
		//判断 如果是删除  则获取beforlist
		if (eventType == CanalEntry.EventType.DELETE) {
			for (CanalEntry.Column column : rowData.getBeforeColumnsList()) {
				if (column.getName().equalsIgnoreCase("category_id")) {
					categoryId = column.getValue();
					return categoryId;
				}
			}
		} else {
			//判断 如果是添加 或者是更新 获取afterlist
			for (CanalEntry.Column column : rowData.getAfterColumnsList()) {
				if (column.getName().equalsIgnoreCase("category_id")) {
					categoryId = column.getValue();
					return categoryId;
				}
			}
		}
		return categoryId;
	}
}

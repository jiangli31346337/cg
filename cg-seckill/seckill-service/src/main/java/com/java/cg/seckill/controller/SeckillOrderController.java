package com.java.cg.seckill.controller;

import java.util.Arrays;
import java.util.Map;

import com.java.cg.seckill.entity.SeckillStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.java.cg.seckill.entity.SeckillOrder;
import com.java.cg.seckill.service.SeckillOrderService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;


/**
 * @author jiangli
 * @since 2020-02-20 14:53:48
 */
@RestController
@RequestMapping("seckill/seckillorder")
public class SeckillOrderController {
	@Autowired
	private SeckillOrderService seckillOrderService;

	/**
	 * 添加秒杀订单
	 */
	@RequestMapping("/add")
	public R add(String time, Long id) {
		//1.获取当前登录的用户的名称
		String username = "szitheima";//测试用写死
		//2.调用service的方法创建订单
		boolean flag = seckillOrderService.add(id, time, username);
		if (flag) {
			return R.ok("正在排队中.....");
		}
		return R.error("秒杀下单失败");
	}

	/*
   查询当前登录的用户的抢单信息(状态)
	*/
	@GetMapping("/query")
	public R queryStatus() {
		String username = "szitheima";//测试用写死
		SeckillStatus seckillStatus = seckillOrderService.queryStatus(username);
		return R.ok(seckillStatus);
	}

	/**
	 * 列表
	 */
	@RequestMapping("/list")
	public R list(@RequestParam Map<String, Object> params) {
		PageUtils page = seckillOrderService.queryPage(params);

		return R.ok().put("page", page);
	}


	/**
	 * 信息
	 */
	@RequestMapping("/info/{id}")
	public R info(@PathVariable("id") Long id) {
		SeckillOrder seckillOrder = seckillOrderService.getById(id);

		return R.ok().put("seckillOrder", seckillOrder);
	}

	/**
	 * 保存
	 */
	@RequestMapping("/save")
	public R save(@RequestBody SeckillOrder seckillOrder) {
		seckillOrderService.save(seckillOrder);

		return R.ok();
	}

	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public R update(@RequestBody SeckillOrder seckillOrder) {
		seckillOrderService.updateById(seckillOrder);

		return R.ok();
	}

	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	public R delete(@RequestBody Long[] ids) {
		seckillOrderService.removeByIds(Arrays.asList(ids));

		return R.ok();
	}

}

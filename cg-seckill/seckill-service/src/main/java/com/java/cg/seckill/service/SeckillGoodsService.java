package com.java.cg.seckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.common.utils.PageUtils;
import com.java.cg.seckill.entity.SeckillGoods;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author jiangli
 * @since 2020-02-20 14:53:48
 */
public interface SeckillGoodsService extends IService<SeckillGoods> {

    PageUtils queryPage(Map<String, Object> params);

	List<SeckillGoods> listByTime(String time);

	SeckillGoods one(String time, Long id);
}


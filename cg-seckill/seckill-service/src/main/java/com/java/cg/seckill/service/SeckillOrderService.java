package com.java.cg.seckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.cg.seckill.entity.SeckillStatus;
import com.java.common.utils.PageUtils;
import com.java.cg.seckill.entity.SeckillOrder;

import java.util.Map;

/**
 * 
 *
 * @author jiangli
 * @since 2020-02-20 14:53:48
 */
public interface SeckillOrderService extends IService<SeckillOrder> {

    PageUtils queryPage(Map<String, Object> params);

	boolean add(Long id, String time, String username);

	SeckillStatus queryStatus(String username);
}


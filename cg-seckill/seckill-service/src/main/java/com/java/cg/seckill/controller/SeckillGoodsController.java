package com.java.cg.seckill.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.java.cg.seckill.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.java.cg.seckill.entity.SeckillGoods;
import com.java.cg.seckill.service.SeckillGoodsService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;


/**
 * @author jiangli
 * @since 2020-02-20 14:53:48
 */
@RestController
@RequestMapping("seckill/seckillgoods")
public class SeckillGoodsController {
    @Autowired
    private SeckillGoodsService seckillGoodsService;

	/**
	 * 根据时间段 和秒杀商品的ID获取商的数据
	 */
	@GetMapping("/one")
	public SeckillGoods one(String time,Long id){
		return seckillGoodsService.one(time,id);
	}

	/**
	 * 获取当前的时间基准的5个时间段
	 */
	@GetMapping("/menus")
	public List<Date> datemenus() {
		return DateUtil.getDateMenus();
	}

	/**
	 * 根据时间段(2019090516) 查询该时间段的所有的秒杀的商品
	 */
	@RequestMapping("/times")
	public List<SeckillGoods> listByTime(String time){
		return seckillGoodsService.listByTime(time);
	}

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = seckillGoodsService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		SeckillGoods seckillGoods = seckillGoodsService.getById(id);

        return R.ok().put("seckillGoods", seckillGoods);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody SeckillGoods seckillGoods){
		seckillGoodsService.save(seckillGoods);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody SeckillGoods seckillGoods){
		seckillGoodsService.updateById(seckillGoods);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		seckillGoodsService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}

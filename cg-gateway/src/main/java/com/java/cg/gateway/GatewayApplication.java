package com.java.cg.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Mono;

/**
 * @author jiangli
 * @since 2020/2/15 20:44
 */
@SpringBootApplication
public class GatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class,args);
	}

	/***
	 * IP限流
	 */
	@Bean(name = "ipKeyResolver")
	public KeyResolver keyResolver(){
		return exchange -> {
			// 获取远程客户端IP
			String hostAddress = exchange.getRequest().getRemoteAddress().getAddress().getHostAddress();
			return Mono.just(hostAddress);
		};
	}
}

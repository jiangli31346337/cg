package com.java.cg.user.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-15 21:54:30
 */
@Data
@TableName("tb_address")
public class Address implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 用户名
	 */
	private String username;
	/**
	 * 省
	 */
	private String provinceid;
	/**
	 * 市
	 */
	private String cityid;
	/**
	 * 县/区
	 */
	private String areaid;
	/**
	 * 电话
	 */
	private String phone;
	/**
	 * 详细地址
	 */
	private String address;
	/**
	 * 联系人
	 */
	private String contact;
	/**
	 * 是否是默认 1默认 0否
	 */
	private String isDefault;
	/**
	 * 别名
	 */
	private String alias;

}

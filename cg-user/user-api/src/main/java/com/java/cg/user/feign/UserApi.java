package com.java.cg.user.feign;

import com.java.cg.user.entity.Address;
import com.java.cg.user.entity.User;
import com.java.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author jiangli
 * @since 2020/2/16 17:30
 */
@FeignClient("cg-user")
public interface UserApi {

	@GetMapping("user/user/load/{username}")
	User findUserById(@PathVariable("username") String username);

	/**
	 * 根据用户名查询收货地址
	 */
	@GetMapping("user/address/{username}")
	List<Address> getAddressByUsername(@PathVariable("username") String username);

	/**
	 * 添加积分
	 */
	@GetMapping("/points/add")
	R addPoints(@RequestParam("points") Integer points);
}

package com.java.cg.user.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 行政区域地州市信息表
 * 
 * @author jiangli
 * @since 2020-02-15 21:54:30
 */
@Data
@TableName("tb_cities")
public class Cities implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 城市ID
	 */
	@TableId
	private String cityid;
	/**
	 * 城市名称
	 */
	private String city;
	/**
	 * 省份ID
	 */
	private String provinceid;

}

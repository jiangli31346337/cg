package com.java.cg.user.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 行政区域县区信息表
 * 
 * @author jiangli
 * @since 2020-02-15 21:54:30
 */
@Data
@TableName("tb_areas")
public class Areas implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 区域ID
	 */
	@TableId
	private String areaid;
	/**
	 * 区域名称
	 */
	private String area;
	/**
	 * 城市ID
	 */
	private String cityid;

}

package com.java.cg.user.service.impl;

import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.user.dao.UserDao;
import com.java.cg.user.entity.User;
import com.java.cg.user.service.UserService;

@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		IPage<User> page = this.page(
				new Query<User>().getPage(params),
				new QueryWrapper<>()
		);

		return new PageUtils(page);
	}

	@Override
	public void addPoints(String username, Integer points) {
		this.baseMapper.addPoints(points, username);
	}

}
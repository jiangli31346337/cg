package com.java.cg.user.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.user.dao.CitiesDao;
import com.java.cg.user.entity.Cities;
import com.java.cg.user.service.CitiesService;

@Service("citiesService")
public class CitiesServiceImpl extends ServiceImpl<CitiesDao, Cities> implements CitiesService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Cities> page = this.page(
                new Query<Cities>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}
package com.java.cg.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.common.utils.PageUtils;
import com.java.cg.user.entity.OauthClientDetails;

import java.util.Map;

/**
 * 
 *
 * @author jiangli
 * @since 2020-02-15 21:54:29
 */
public interface OauthClientDetailsService extends IService<OauthClientDetails> {

    PageUtils queryPage(Map<String, Object> params);
}


package com.java.cg.user.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.user.dao.AddressDao;
import com.java.cg.user.entity.Address;
import com.java.cg.user.service.AddressService;

@Service("addressService")
public class AddressServiceImpl extends ServiceImpl<AddressDao, Address> implements AddressService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Address> page = this.page(
                new Query<Address>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}
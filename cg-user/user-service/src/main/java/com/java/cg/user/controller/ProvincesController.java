package com.java.cg.user.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.cg.user.entity.Provinces;
import com.java.cg.user.service.ProvincesService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 省份信息表
 *
 * @author jiangli
 * @since 2020-02-15 21:54:30
 */
@RestController
@RequestMapping("user/provinces")
public class ProvincesController {
    @Autowired
    private ProvincesService provincesService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = provincesService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{provinceid}")
    public R info(@PathVariable("provinceid") String provinceid){
		Provinces provinces = provincesService.getById(provinceid);

        return R.ok().put("provinces", provinces);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody Provinces provinces){
		provincesService.save(provinces);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody Provinces provinces){
		provincesService.updateById(provinces);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] provinceids){
		provincesService.removeByIds(Arrays.asList(provinceids));

        return R.ok();
    }

}

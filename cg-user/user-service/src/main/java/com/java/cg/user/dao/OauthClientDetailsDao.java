package com.java.cg.user.dao;

import com.java.cg.user.entity.OauthClientDetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-15 21:54:29
 */
@Mapper
public interface OauthClientDetailsDao extends BaseMapper<OauthClientDetails> {
	
}

package com.java.cg.content.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 内容分类
 * 
 * @author jiangli
 * @since 2020-02-14 15:00:12
 */
@Data
@TableName("tb_content_category")
public class ContentCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 类目ID
	 */
	@TableId
	private Long id;
	/**
	 * 分类名称
	 */
	private String name;

}

package com.java.cg.content.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.content.dao.ContentDao;
import com.java.cg.content.entity.Content;
import com.java.cg.content.service.ContentService;

@Service("contentService")
public class ContentServiceImpl extends ServiceImpl<ContentDao, Content> implements ContentService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Content> page = this.page(
                new Query<Content>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

	/***
	 * 根据分类ID查询
	 * @param id
	 * @return
	 */
	@Override
	public List<Content> findByCategory(Long id) {
		return this.baseMapper.selectList(new LambdaQueryWrapper<Content>()
		.eq(Content::getCategoryId,id).eq(Content::getStatus,"1"));
	}

}
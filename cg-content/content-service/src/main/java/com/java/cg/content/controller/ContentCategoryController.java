package com.java.cg.content.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.cg.content.entity.ContentCategory;
import com.java.cg.content.service.ContentCategoryService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 内容分类
 *
 * @author jiangli
 * @since 2020-02-14 15:00:12
 */
@RestController
@RequestMapping("content/contentcategory")
public class ContentCategoryController {
    @Autowired
    private ContentCategoryService contentCategoryService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = contentCategoryService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		ContentCategory contentCategory = contentCategoryService.getById(id);

        return R.ok().put("contentCategory", contentCategory);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody ContentCategory contentCategory){
		contentCategoryService.save(contentCategory);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody ContentCategory contentCategory){
		contentCategoryService.updateById(contentCategory);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		contentCategoryService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}

package com.java.cg.content.dao;

import com.java.cg.content.entity.ContentCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 内容分类
 * 
 * @author jiangli
 * @since 2020-02-14 15:00:12
 */
@Mapper
public interface ContentCategoryDao extends BaseMapper<ContentCategory> {
	
}

package com.java.cg.content.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.common.utils.PageUtils;
import com.java.cg.content.entity.ContentCategory;

import java.util.Map;

/**
 * 内容分类
 *
 * @author jiangli
 * @since 2020-02-14 15:00:12
 */
public interface ContentCategoryService extends IService<ContentCategory> {

    PageUtils queryPage(Map<String, Object> params);
}


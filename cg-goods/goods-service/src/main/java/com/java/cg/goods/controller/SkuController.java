package com.java.cg.goods.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.java.cg.goods.entity.Sku;
import com.java.cg.goods.service.SkuService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 商品表
 *
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@RestController
@RequestMapping("goods/sku")
public class SkuController {
	@Autowired
	private SkuService skuService;

	/**
	 * 减库存
	 *
	 * @return
	 */
	@GetMapping("/decr")
	public R decr(@RequestParam("decrMap") Map<String, Integer> decrMap) {
		skuService.decrCount(decrMap);
		return R.ok();
	}

	/**
	 * 根据spuId查询skus
	 */
	@GetMapping("list/{spuId}")
	public List<Sku> getSkusBySpuId(@PathVariable("spuId") Long spuId) {
		return skuService.list(new LambdaQueryWrapper<Sku>().eq(Sku::getSpuId, spuId).eq(Sku::getStatus, "1"));
	}

	/**
	 * 列表
	 */
	@RequestMapping("/search/{page}/{size}")
	public List<Sku> search(@RequestParam("page") Integer page, @RequestParam("size") Integer size, @RequestBody(required = false) Sku sku) {
		PageUtils<Sku> pageUtils = skuService.queryPage(page, size, sku);
		return pageUtils.getList();
	}


	/**
	 * 信息
	 */
	@GetMapping("/info/{id}")
	public Sku info(@PathVariable("id") Long id) {
		return skuService.getById(id);
	}

	/**
	 * 保存
	 */
	@RequestMapping("/save")
	public R save(@RequestBody Sku sku) {
		skuService.save(sku);

		return R.ok();
	}

	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public R update(@RequestBody Sku sku) {
		skuService.updateById(sku);

		return R.ok();
	}

	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	public R delete(@RequestBody Long[] ids) {
		skuService.removeByIds(Arrays.asList(ids));

		return R.ok();
	}

}

package com.java.cg.goods.dao;

import com.java.cg.goods.entity.Spec;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@Mapper
public interface SpecDao extends BaseMapper<Spec> {

	@Select("SELECT ts.* FROM `tb_spec` ts INNER JOIN `tb_category` tc ON ts.`template_id` = tc.`template_id` WHERE tc.`id` = #{categoryId}")
	List<Spec> findByCategoryId(Integer categoryId);
}

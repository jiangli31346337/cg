package com.java.cg.goods.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.goods.dao.TemplateDao;
import com.java.cg.goods.entity.Template;
import com.java.cg.goods.service.TemplateService;

@Service("templateService")
public class TemplateServiceImpl extends ServiceImpl<TemplateDao, Template> implements TemplateService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Template> page = this.page(
                new Query<Template>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}
package com.java.cg.goods.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.goods.dao.ParaDao;
import com.java.cg.goods.entity.Para;
import com.java.cg.goods.service.ParaService;

@Service("paraService")
public class ParaServiceImpl extends ServiceImpl<ParaDao, Para> implements ParaService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Para> page = this.page(
                new Query<Para>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

	@Override
	public List<Para> findByCategoryId(Integer categoryId) {

		return null;
	}

}
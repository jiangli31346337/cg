package com.java.cg.goods.dao;

import com.java.cg.goods.entity.Brand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 品牌表
 * 
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@Mapper
public interface BrandDao extends BaseMapper<Brand> {

	@Select("SELECT tb.* FROM `tb_brand` tb INNER JOIN `tb_category_brand` tcb ON tb.`id`=tcb.`brand_id` WHERE tcb.`category_id` = #{categoryId}")
	List<Brand> findByCategoryId(Integer categoryId);

}

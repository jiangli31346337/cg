package com.java.cg.goods.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.cg.goods.dao.SpuDao;
import com.java.cg.goods.entity.*;
import com.java.cg.goods.service.BrandService;
import com.java.cg.goods.service.CategoryService;
import com.java.cg.goods.service.SkuService;
import com.java.cg.goods.service.SpuService;
import com.java.common.exception.RRException;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("spuService")
public class SpuServiceImpl extends ServiceImpl<SpuDao, Spu> implements SpuService {
	@Autowired
	private SkuService skuService;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private BrandService brandService;

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		IPage<Spu> page = this.page(
				new Query<Spu>().getPage(params),
				new QueryWrapper<>()
		);

		return new PageUtils(page);
	}

	/**
	 * 添加商品
	 */
	@Override
	public void saveGoods(Goods goods) {
		Spu spu = goods.getSpu();

		// 判断spuId是否为空
		if (spu.getId() == null) {
			// 为空,则新增spu
			spu.setId(IdWorker.getId());
			this.baseMapper.insert(spu);
		}else {
			// 否则修改spu并删除之前的skus
			this.baseMapper.updateById(spu);
			skuService.remove(new LambdaQueryWrapper<Sku>().eq(Sku::getSpuId,spu.getId()));
		}

		List<Sku> skus = goods.getSkus();
		Category category = categoryService.getById(spu.getCategory3Id());
		Brand brand = brandService.getById(spu.getBrandId());
		Date date = new Date();
		skus.forEach(sku -> {
			sku.setId(IdWorker.getId());
			// skuName = spuName + " " + 规格信息
			StringBuilder name = new StringBuilder(spu.getName());
			String spec = sku.getSpec();
			if (StrUtil.isNotEmpty(spec)) {
				name.append(" ");
				Map<String, String> map = JSONUtil.toBean(spec, Map.class);
				for (Map.Entry<String, String> entry : map.entrySet()) {
					name.append(entry.getValue());
				}
			}
			sku.setName(name.toString());
			sku.setCreateTime(date);
			sku.setUpdateTime(date);
			sku.setSpuId(spu.getId());
			sku.setCategoryId(category.getId()); // 3级分类id
			sku.setCategoryName(category.getName()); // 3级分类名称
			sku.setBrandName(brand.getName());

		});
		skuService.saveBatch(skus);
	}

	@Override
	public Goods findGoodsBySpuId(Integer spuId) {
		Spu spu = this.baseMapper.selectById(spuId);

		List<Sku> skus = skuService.list(new LambdaQueryWrapper<Sku>().eq(Sku::getSpuId, spu.getId()));

		Goods goods = new Goods();
		goods.setSpu(spu);
		goods.setSkus(skus);
		return goods;
	}

	@Override
	public void audit(Integer spuId) {
		Spu spu = this.baseMapper.selectById(spuId);
		// 已经删除 || 状态不是未审核
		if (spu.getIsDelete() || !StrUtil.equals("0", spu.getStatus())) {
			throw new RRException("当前商品不能审核!");
		}

		spu.setStatus("1");
		spu.setIsMarketable("1");
		this.baseMapper.updateById(spu);
	}

	@Override
	public void pull(Integer spuId) {
		Spu spu = this.baseMapper.selectById(spuId);
		// 已经删除
		if (spu.getIsDelete() ) {
			throw new RRException("当前商品不能下架!");
		}

		spu.setIsMarketable("0");
		this.baseMapper.updateById(spu);
	}

	@Override
	public void put(Integer spuId) {
		Spu spu = this.baseMapper.selectById(spuId);
		// 已经删除 || 状态不是未审核
		if (spu.getIsDelete() || !StrUtil.equals("1", spu.getStatus())) {
			throw new RRException("当前商品不能上架!");
		}
		spu.setIsMarketable("1");
		this.baseMapper.updateById(spu);
	}

}
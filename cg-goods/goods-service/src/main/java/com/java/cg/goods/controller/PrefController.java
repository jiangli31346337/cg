package com.java.cg.goods.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.cg.goods.entity.Pref;
import com.java.cg.goods.service.PrefService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 
 *
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@RestController
@RequestMapping("goods/pref")
public class PrefController {
    @Autowired
    private PrefService prefService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = prefService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id){
		Pref pref = prefService.getById(id);

        return R.ok().put("pref", pref);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody Pref pref){
		prefService.save(pref);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody Pref pref){
		prefService.updateById(pref);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] ids){
		prefService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}

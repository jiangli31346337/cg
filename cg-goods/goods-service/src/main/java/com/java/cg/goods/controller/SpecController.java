package com.java.cg.goods.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.java.cg.goods.entity.Spec;
import com.java.cg.goods.service.SpecService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 
 *
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@RestController
@RequestMapping("goods/spec")
public class SpecController {
    @Autowired
    private SpecService specService;

	@GetMapping("/category/{categoryId}")
	public R findByCategoryId(@PathVariable("categoryId")Integer categoryId) {
		List<Spec> specs = specService.findByCategoryId(categoryId);
		return R.ok(specs);
	}

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = specService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id){
		Spec spec = specService.getById(id);

        return R.ok().put("spec", spec);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody Spec spec){
		specService.save(spec);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody Spec spec){
		specService.updateById(spec);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] ids){
		specService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}

package com.java.cg.goods.dao;

import com.java.cg.goods.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品类目
 * 
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@Mapper
public interface CategoryDao extends BaseMapper<Category> {
	
}

package com.java.cg.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.cg.goods.entity.Sku;
import com.java.common.utils.PageUtils;

import java.util.Map;

/**
 * 商品表
 *
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
public interface SkuService extends IService<Sku> {

	PageUtils<Sku> queryPage(Integer page, Integer size, Sku sku);

	void decrCount(Map<String, Integer> decrMap);
}


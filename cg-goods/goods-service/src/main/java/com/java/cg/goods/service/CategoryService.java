package com.java.cg.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.common.utils.PageUtils;
import com.java.cg.goods.entity.Category;

import java.util.List;
import java.util.Map;

/**
 * 商品类目
 *
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
public interface CategoryService extends IService<Category> {

    PageUtils queryPage(Map<String, Object> params);

	List<Category> findByPid(Integer parentId);
}


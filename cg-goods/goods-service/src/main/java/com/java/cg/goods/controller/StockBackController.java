package com.java.cg.goods.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.cg.goods.entity.StockBack;
import com.java.cg.goods.service.StockBackService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 
 *
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@RestController
@RequestMapping("goods/stockback")
public class StockBackController {
    @Autowired
    private StockBackService stockBackService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = stockBackService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{orderId}")
    public R info(@PathVariable("orderId") String orderId){
		StockBack stockBack = stockBackService.getById(orderId);

        return R.ok().put("stockBack", stockBack);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody StockBack stockBack){
		stockBackService.save(stockBack);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody StockBack stockBack){
		stockBackService.updateById(stockBack);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] orderIds){
		stockBackService.removeByIds(Arrays.asList(orderIds));

        return R.ok();
    }

}

package com.java.cg.goods.dao;

import com.java.cg.goods.entity.Para;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@Mapper
public interface ParaDao extends BaseMapper<Para> {

	@Select("SELECT tp.* FROM `tb_para` tp INNER JOIN `tb_category` tc ON tp.`template_id` = tc.`template_id` WHERE tc.`id` = #{categoryId}")
	List<Para> findByCategoryId(Integer categoryId);
	
}

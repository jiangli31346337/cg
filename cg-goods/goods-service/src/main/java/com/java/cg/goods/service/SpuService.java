package com.java.cg.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.cg.goods.entity.Goods;
import com.java.common.utils.PageUtils;
import com.java.cg.goods.entity.Spu;

import java.util.Map;

/**
 * 
 *
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
public interface SpuService extends IService<Spu> {

    PageUtils queryPage(Map<String, Object> params);

	void saveGoods(Goods goods);

	Goods findGoodsBySpuId(Integer spuId);

	void audit(Integer spuId);

	void pull(Integer spuId);

	void put(Integer spuId);
}


package com.java.cg.pay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author jiangli
 * @since 2020/2/19 14:31
 * 文档https://pay.weixin.qq.com/wiki/doc/api/native.php?chapter=6_5
 */
@SpringBootApplication
@EnableFeignClients
public class PayApplication {

	public static void main(String[] args) {
		SpringApplication.run(PayApplication.class,args);
	}
}
